"use strict";

function tscWrapper(grunt, options) {
  var exitCode = 0;

  function fakeExit(code) {
    exitCode = code;
  }

  var fakeProc = {};

  var keys = Object.keys(process);

  if (options.debug) {
    grunt.log.debug("process keys: " + JSON.stringify(Object.keys(process)));
  }

  for (var n = 0; n < keys.length; n++) {
    var key = keys[n];
    fakeProc[key] = process[key];
  }

  if (options.debug) {
    grunt.log.debug("fakeProc keys: " + JSON.stringify(Object.keys(fakeProc)));
  }

  fakeProc.argv = ["node", "tsc"].concat(options.args);
  fakeProc.exit = fakeExit;

  var oldProc = process;
  process = fakeProc;

  require('typescript/lib/tsc.js');

  process = oldProc;

  return exitCode;
}

function invokeTsc(grunt, self) {
  self.done = self.async();
  
  var options = self.options({
    args: ["-p", "tsconfig.json"],
    debug: false
  });

  if (options.debug) {
    grunt.log.debug("calling tsc with options " + JSON.stringify(options.args));
  }

  var exitCode = tscWrapper(grunt, options);

  if (options.debug) {
    grunt.log.debug("tsc ended, called with options " + JSON.stringify(options.args));
  }

  self.done(exitCode === 0);
}

function registerTscTask(grunt) {
  grunt.registerMultiTask("tsc", "invokes typescript compiler (tsc) from node without forking, like from command line", function() {
    invokeTsc(grunt, this);
  });
}

module.exports = registerTscTask;
