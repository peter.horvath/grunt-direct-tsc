# grunt-direct-tsc
Grunt task to call tsc, as from the command line, but without forking a new
node instance.

Watchmodus (`-w`) doesn't work yet, but if you file an issue, I will implement
it in days.

### Example:

    module.exports = function(grunt) {
      'use strict';
      grunt.initConfig({
    
        tsc : {
          dist : {
            options : {
              args : [ "-p", "tsconfig-dist.json" ],
              debug : true
            }
          },
          test : {
            options : {
              args : [ "-p", "tsconfig-test.json" ],
              debug : true
            }
          }
        }

      });
      grunt.loadNpmTasks("grunt-direct-tsc");
    };

Here we can see multiple configurations in which we can call the typescript
compiler. I think it is nearly self-explaining:

`grunt tsc:dist` will be the equivalent of a `./node_modules/.bin/tsc -p tsconfig-dist.json`,
while `grunt tsc:dev` will do an equivalent of `./node_modules/.bin/tsc -p tsconfig-test.json`.

There are no more configuration options.
